# Get all currect SSH keys - test
data "hcloud_ssh_key" "matej" {
  fingerprint = "96:7e:05:c3:87:f0:4b:bd:e0:66:36:4f:ef:5d:44:69"
}
data "hcloud_ssh_key" "david" {
  fingerprint = "35:a8:0f:9d:39:3d:81:ae:b9:54:c8:6e:3f:78:96:ec"
}
data "hcloud_ssh_key" "lukas" {
  fingerprint = "a8:75:6c:a7:e3:5d:5b:a6:b3:76:5b:ae:2d:e8:5f:d2"
}

# Create terraform network
resource "hcloud_network" "terraform_network" {
  name     = "network"
  ip_range = "10.20.0.0/16"
}

resource "hcloud_network_subnet" "terraform_subnet" {
  type         = "cloud"
  network_id   = hcloud_network.terraform_network.id
  network_zone = "eu-central"
  ip_range     = "10.20.1.0/24"
}

# Create master node
resource "hcloud_server" "master" {
  name        = "terraform-master"
  server_type = "cpx21"
  image       = "ubuntu-20.04"
  location    = "hel1"
  ssh_keys    = [data.hcloud_ssh_key.matej.id, data.hcloud_ssh_key.david.id, data.hcloud_ssh_key.lukas.id]

# Create network
  network {
    network_id = hcloud_network.terraform_network.id
    ip         = "10.20.1.2"
  }

  depends_on = [
    hcloud_network_subnet.terraform_subnet,
    data.hcloud_ssh_key.matej,
    data.hcloud_ssh_key.david,
    data.hcloud_ssh_key.lukas
  ]
}

# Create worker nodes
resource "hcloud_server" "workers" {
  count       = var.n_workers
  name        = "terraform-worker-${count.index + 1}"
  server_type = "cpx21"
  image       = "ubuntu-20.04"
  location    = "hel1"
  ssh_keys    = [data.hcloud_ssh_key.matej.id, data.hcloud_ssh_key.david.id, data.hcloud_ssh_key.lukas.id]

  network {
    network_id = hcloud_network.terraform_network.id
    # last octet in IP address starts from .3
    ip         = "10.20.1.${count.index + 3}"
  }

  depends_on = [
    hcloud_network_subnet.terraform_subnet,
    data.hcloud_ssh_key.matej,
    data.hcloud_ssh_key.david,
    data.hcloud_ssh_key.lukas
  ]
}

