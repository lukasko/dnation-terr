# hcloud token is stored in env variable HCLOUD_TOKEN
terraform {
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
    }
  }
}
